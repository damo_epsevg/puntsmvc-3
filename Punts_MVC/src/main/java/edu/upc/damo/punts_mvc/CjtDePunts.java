package edu.upc.damo.punts_mvc;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Josep M on 30/09/13.
 */
public class CjtDePunts implements Iterable<Punt> {

   /* Interfície que han d'implementar els observadors */
    public interface CanviCjtDePuntsListener {
        void onCanviCjtDePunts();
    }

    /* Magatzem de dades */
    private final List<Punt> punts = new LinkedList<Punt>();

    /* Observador */
    private CanviCjtDePuntsListener observador;

    /* --------------- Mètodes públics ------------------------- */
     CjtDePunts(){
     }


    /* Enregistrament de l'observador */

    public void setCjtDePuntsListener(CanviCjtDePuntsListener o)
    {observador = o;}


    public void afegeixPunt(Punt p)
    { punts.add(p);
      avisaObservador();
    }


    /* --------------- Mètodes privats ------------------------- */


    private void avisaObservador(){
        if(observador!=null){observador.onCanviCjtDePunts();}
    }

    @Override
    public Iterator<Punt> iterator() {
       return punts.listIterator();

    }



}
